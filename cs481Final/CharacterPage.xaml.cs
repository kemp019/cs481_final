﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace cs481Final
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CharacterPage : ContentPage
    {
        //default constructor
        public CharacterPage()
        {
            InitializeComponent();
        }
        //our awesome constructor that allows us to use pre-created templates like a baller
        public CharacterPage(GameCharacter newCharacter)
        {
            InitializeComponent();
            this.Title = newCharacter.Name;
            BindingContext = newCharacter;
        }
     
        //takes us back to the main menu once we are all done here
        private async void BackButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopToRootAsync();
        }
    }
}