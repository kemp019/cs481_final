﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace cs481Final
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SpecialPageOwl1 : ContentPage
    {

        public SpecialPageOwl1()
        {
            InitializeComponent();
        }

        //The Owl is a cheating bastard who uses homework 6 to try to come out on top.
        private async void OwlBar_Completed(object sender, EventArgs e)
        {

            OwlActivity.Text = "The Owl makes strange movements with its wings."; //update text to show the user that the owl is searching the word.

            if (!Utility.CheckConnected()) //do we not have internet?
            {
                //no internet, the Owl cannot use his phone to cheat
                Utility.currentTextPage.BuildTextPage(7); //go to owl caught page
                await Navigation.PopAsync();
            }

            var text = ((Entry)sender).Text; //our searched text

            if (text == "Grandma") //is the secret route unlocked?
            {
                Utility.currentTextPage.BuildTextPage(10); //go to the Cat page
                await Navigation.PopAsync();
            }

            HttpClient client = new HttpClient();
            var uri = new Uri(string.Format($"https://owlbot.info/api/v2/dictionary/" + $"{text}"));
            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = uri;

            HttpResponseMessage response = await client.SendAsync(request);

            if (response.IsSuccessStatusCode) //successfully got a definition. The owl wins.
            {
                var content = await response.Content.ReadAsStringAsync(); //transcribe recieved knowledge
                var wordDefinition = WordDefinition.FromJson(content); //render it safe for comprehension by mortal minds.

                Utility.PageTexts[8].BodyText = Utility.PageTexts[8].BodyText.Replace("[word]", text); //make page 8's text include the word
                Utility.PageTexts[8].BodyText = Utility.PageTexts[8].BodyText.Replace("[definition]", wordDefinition[0].Definition); //make page 8's text include the definition

                Utility.currentTextPage.BuildTextPage(8);
                await Navigation.PopAsync();
            }
            else //couldn't get a response for the word. This means the player beats the owl.
            {
                Utility.currentTextPage.BuildTextPage(9);
                await Navigation.PopAsync();
            }
        }
    }
}