﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using Plugin.Connectivity;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace cs481Final
{
    //misc useful stuff. and page-independent variables
    static class Utility
    {
        //the current textpage.
        public static TextPage currentTextPage;

        //list of all gamecharacters
        public static List<GameCharacter> GameCharacters = new List<GameCharacter>();
        //list of all PageTexts
        public static List<PageText> PageTexts = new List<PageText>();

        //creates all the characters. 
        public static void BuildGameCharacters()
        {
            string charDesc;
            GameCharacter gameChar;

            //0 The Protagonist
            charDesc = "The Protagonist is the simplest character to discuss. The protagonist is generally treated as an empty vessel for the reader to assume, and as such possesses rather limited characterization. No physical appearance is specified or elaborated upon, and other characters tend not to comment on those aspects of the protagonist, leaving much about them shrouded in mystery."
                + "\n\nUnlike the other characters, the Protagonist does not have a set personality. What personality does exist is often one of vague bemusement at the situation, and a degree of exasperation with the writing. The protagonist often sees the writing as notably bad, fearing in one instance that it would result in receiving a \"low effort\" deduction."
                + "\n\nNo career or occupation is specified for the Protagonist, though they are likely simply an actor playing a role as is the case for the Snake and the Spider. It is worth noting that the Protagonist may have some knowledge of the sport of boxing, and has recently won a championship belt previously held by the Snake. Weight class is, of course, completely unknown.";
            gameChar = new GameCharacter(true, "The Protagonist", "This one's you", charDesc);
            cs481Final.Utility.GameCharacters.Add(gameChar);

            //1 The Snake
            charDesc = "The Snake is a somewhat more complex character than the Protagonist. While thus far it has primarily played the role of antagonist, the Snake does not seem to be morally opposed to the Protagonist and is an obstacle soley due to that being its job. It even apoligizes before attacking in one instance, saying that it is just \"following the script\". Nonetheless, the Snake is a formidable obstacle. The Snake is very large for a snake, capable of blocking a street. It is green, as it is a snake, and possesses glowing red eyes. Off-set interviews have established that these may be contact lenses however."
                + "\n\nThe Snake's personality is likewise more complex than the Protagonist. The Snake is willing to be friendly, and often greets people it comes across. However, the Snake is also very impatient about being ignored, seeing such behaviour as very rude. While well-equipped for battle with poisonous fangs, the Snake seems to have a sort of \"martial honor\" and prefers to engage in boxing matches to resolve conflicts. While the Snake has lost both fist fights it has engaged in due to a lack of arms, it clearly has some prowess as a boxer as it is a former champion. A notable trait of the Snake is its fear of streets. This is due to seeing several of its siblings ran over by cars as a child, as few people who would swerve for a squirrel do the same for a snake."
                + "\n\nThe Snake is an actor playing a role, as evidenced by comments referring to a script. It was, however, a champion boxer before losing its belt to the protagonist. If certain lines are to be believed, the Snake has a seperate boxing career which it is now focusing on after its loss.";
            gameChar = new GameCharacter(false, "The Snake", "A scaled slitherer", charDesc);
            cs481Final.Utility.GameCharacters.Add(gameChar);

            //2 The Spider
            charDesc = "The Spider is an ally to the Protagonist, and serves as philosophical advisor and convenient rescuer. He is a formidable combatant, possessing eight arms which makes him very good at punching. His carapace is black."
                + "\n\nThe Spider is a laid back and philosophical character for the most part. He bemoans his typecasting since playing a stunt role in the Lord of the Rings, though he does not bemoan dating his co-star, who he is currently living with. He was proud to have gotten a speaking role in the Harry Potter movies, which is also where he first worked alongside the Snake. The Spider is well read, and has written two essays on the writings of Marcus Aurellius, who forms the basis of his own personal philosophy. He often offers advice to his costars, using his philosophy as a basis."
                + "\n\nThe Spider works as an actor, though he once worked a second job in a clothing factory as a \"knitting specialist\". He is active in his community, promoting himself in roles that help spiders break free of stereotypes. He has received rewards from both spiders and scorpions for his work on rehabilating the public image of arachnids everywhere, and was a major donator towards the Greek Spider Movement's Arachne Memorial. He has been an ardent fan of Conleth Hill after his role as Varys \"The Spider\" in the television series \"Game of Thrones\".";
            gameChar = new GameCharacter(false, "The Spider", "A wise weaver", charDesc);
            cs481Final.Utility.GameCharacters.Add(gameChar);

            //3 The Owl
            charDesc = "The Owl is the final obstacle to the Protagonist, and is a dangerous foe. Highly resourceful, the Owl augments his natural advantages with modern technology. He serves as a foil to the Snake. While both he and the Snake oppose the Protagonist, the Snake does so with honor. The Owl instead chooses to cheat, preferring instead an unearned victory to a fairly reached defeat."
                + "\n\nThe Owl furthers his unsavory nature by having a noted prejudice against insects and spiders, seeing them as little more than food beneath his notice. He even refuses to acknowledge the difference between insects and arachnids, a stance the Spider finds very offensive. The Owl is characterized by arrogance and a sense of superiority, even when he defaults to cheating to get his way. He believes the Spider to be no match for him, but upon realizing the Spider's true size quickly flees, suggesting something of a self-servign cowardice. The Owl also possesses a cruel streak, exemplified by his plot to keep the Protagonist awake all night."
                + "\n\nDespite all this, it should be noted that the Owl is simply an actor like the rest of the characters. His repungnant personality is merely the role he plays, and he does a good job at it. One should always remember that ultimately the actor and the role are not the same person, and hatred for one should not easily bleed over to be hatred of the other. Off of the stage, the Owl often engages in charity work, often alongside the Spider to lend both of their voices to worthy causes. The Owl also participates in library reading groups, valuing literacy and the learning it brings.";
            gameChar = new GameCharacter(false, "The Owl", "A no-good nocturnal", charDesc);
            cs481Final.Utility.GameCharacters.Add(gameChar);

            //4 The Cat
            charDesc = "The Cat exists";
            gameChar = new GameCharacter(false, "The Cat", "Nine lives within one eye", charDesc);
            cs481Final.Utility.GameCharacters.Add(gameChar);

            //5 The Grandmother
            charDesc = ":(";
            gameChar = new GameCharacter(false, "The Grandmother", "", charDesc);
            cs481Final.Utility.GameCharacters.Add(gameChar);

        }
 
        //creates all page texts. Prime canidate to implement reading from a file for.
        public static void BuildGameTexts()
        {
            //declare variables
            string bodyText;
            PageText pageText;
            int[] nextPages;

            //0
            bodyText = "Ahead of you, you spy a giant snake. It is giant. Being a snake it is also clearly venomous, because no one ever writes about non-venomous snakes, now do they? You know it is clearly evil because it has glowy red eyes, and is additionally a giant snake. However, the evil giant snake happens to be in your way. Try as you might, you somehow cannot think of more than three possibilities to deal with this scaley problem. Putting aside your concerns over your apparent lack of imagination you take inventory of your solutions."
                + "\n\nYour first thought is to simply go around the snake. [OPTION 1]"
                + "\n\nYour second thought is to try to talk to the snake. [OPTION 2]"
                + "\n\nYour third and final thought is to punch the snake. [OPTION 3]"
                + "\n\nMake your choice.";
            nextPages = new int[] {1, 2, 3};
            pageText = new PageText("Dilemna: Serpentine Stopsign", "Enter the Snake", bodyText, nextPages, 1);
            cs481Final.Utility.PageTexts.Add(pageText);

            //1
            bodyText = "You walk up to the snake. It raises its head, and crimson eyes burn into yours, a blood red beacon drilling into your skull. Its tongue darts from its mouth, accompanined by a rumbling hiss emanating deep from within the serpent's titan coils. Its mouth slowly opens, venom-slicked fangs glinting red and dead from the glow of its baleful eyes."
                + "\n\n\"Sssssup dude?\" The snake says upon your approach."
                + "\n\nYou ignore the snake and try to walk around it. If you do not acknowledge a problem's existence, it clearly does not exist and is thus not a cause for concern, right? With such wisdom, what could possibly oppose you?"
                + "\n\n\"Ssserioussssly man? Itsss kinda rude to just ignore sssomeone like that.\" The snake chides as you continue to walk around it, at an inexplicably leisurely pace."
                + "\n\nNot one to be lectured by reptiles that couldn't even figure out legs, you ignore the snake and finish your circumnavigation. Now on the other side of the reptillian road block, you prepare to be on your way."
                + "\n\n\"Wow. Sssscrew thissss.\" The snake says before lunging at you."
                + "\n\n\"You know, the relation between what the option described and what is happening here is pretty tenuous at best.\" You tell the snake as it coils around you."
                + "\n\n\"Tough sssshit.\" The snake replies, not in the mood to discuss the virtues of writing after you so rudely spurned it."
                + "\n\n\"Seriously this is really obvious railroading and really lazy writing. Like, this is borderline offensive in its badness.\" You complain."
                + "\n\n\"Sssucksss to be you.\" The snake says before biting down."
                + "\n\nAs your veins burn with venom the world blurs and blackens, fading away alongside you. But after an eternity of void you hear the words that confirm your return to the beginning, that send a paralyzing chill down your spine. They echo in your ears, bouncing around your skull as you open your eyes."
                + "\n\n\"Hey, you. You're finally awake.\"";
            nextPages = new int[] {0};
            pageText = new PageText("DEATH: Stray Not From the Path", "whoops", bodyText, nextPages);
            cs481Final.Utility.PageTexts.Add(pageText);

            //2
            bodyText = "You walk up to the snake."
                + "\n\n\"Sssssup dude?\" The snake says upon your approach."
                + "\n\n\"Not much.\" You tell the snake. \"How are you doing today?\""
                + "\n\n\"Been better. Thisss job has me sssiting in the ssstreet which is pretty dangerousss." + '"' + " The snake replies."
                + "\n\n\"My sympathies." + '"' + " You tell the snake. " + '"' + "On the subject, I kinda need to get by. Since you don't like being in the street and I need to walk there, this should be a win win?" + '"'
                + "\n\n\"Aw. That sssucksss." + '"' + " The snake says."
                + "\n\n\"How come?" + '"' + " You ask."
                + "\n\n\"I'm sssuposssed to kill anyone who ssseeksss to sssurf the ssstreet. It sssucksss caussssse you sssseem pretty decent." + '"'
                + "\n\n\"That seems kinda extreme." + '"' + " You reply. " + '"' + "Think we could make an exception?" + '"'
                + "\n\n\"nope" + '"' + " says the snake."
                + "\n\n\"Thats unfortunate." + '"' + " You tell the snake as it readies itself to attack."
                + "\n\n\"Ssssorry dude. I jusssst read the sssscript. " + '"' + " The snake confesses as it begins to lunge towards you."
                + "\n\nIt is then that a giant spider swings in and body slams the snake. As the two creatures face off the snake shakes its head."
                + "\n\n\"Ssseriousssly thatsss jussst sssstupid." + '"'
                + "\n\nThe spider fights the snake, but its pretty one sided. They are both poisonous, of course, and that obviously means they are immune to poison. So there is no advantage there. However, snakes have no arms so they cannot punch people. Spiders, however, have eight arms meaning they can punch really good. Ultimately the snake didn't have much of a chance and is soundly beaten in round three."
                + "\n\n\"You know, the snake kind of has a point." + '"' + " You tell the spider."
                + "\n\n\"How so ?" + '"' + " The spider replies."
                + "\n\n\"Well, there really wasn't much foreshadowing of a rescue or anything." + '"'
                + "\n\nThe spider shrugs in response, which probably looks pretty cool since it has eight arms." + '"' + "I don't really mind, personally." + '"' + " It says." + '"' + " I've been typecasted since Lord of the Rings, its kinda nice to play something thats not a villain." + '"'
                + "\n\n\"I get that." + '"' + " You tell the spider." + '"' + " But with writing like this we might get that ambiguous 'low effort' deduction slapped on us." + '"'
                + "\n\n\"nah" + '"' + " the spider says." + '"' + "We're good. Its pretty undefined, so we could have ended up with it no matter what we did or how hard we worked. This way we get to have some fun with it." + '"'
                + "\n\n\"Still though, I'm worried about the thing. Not to mention getting sick and coming back to find that all the other reliable members of my group moved to another one." + '"'
                + "\n\n\"Hey man, sometimes shit happens." + '"' + " The spider says." +'"' + " Nothing we can do. And besides, there's a list of things we should have in the program next to the turn in. We'll just put comments by everything we do on the list and we should be good. Its not like we would be required to do work we weren't told to do. Now lets get back to script." + '"'
                + "\n\n\"All right then" + '"' + " You tell the spider." + '"' + " What's next ?" + '"'
                + "\n\n\"Continuing the story, cause we got all sorts of strange new features to try. In another life I might have told you we were done, but this is a new future.\""
                + "\n\n\"Thats nice.\" You tell the spider. \"I was afraid I wouldn't get to see how this story developed.\""
                + "\n\n\"We almost didn't get to continue it, the subsequent homeworks didn't exactly lend themselves to storytelling. But we've got the final and like a week to make it work, we'll have some new tales to tell for sure.\" The spider says reassuringly."
                + "\n\n\"Thats a good point. Anyways, see ya around." + '"' + " you tell the spider."
                + "\n\n\"Toodles my dude." + '"' + " The spider says as it swings away."
                + "\n\nWith this encounter resolved you take a moment to prepare yourself before continuing your journey.";
            nextPages = new int[] { 4 };
            pageText = new PageText("Event: War of Words", "Enter the Spider", bodyText, nextPages, 2);
            cs481Final.Utility.PageTexts.Add(pageText);

            //3
            bodyText = "You walk up to the snake."
                + "\n\n\"Sssssup dude?" + '"' + " The snake says upon your approach."
                + "\n\nWhile you do feel somewhat bad about how you are about to punch out someone with a speech impediment, you put your moral concerns aside and concentrate on the task of defeating the giant snake. Before the creature can react you draw your fist back and land a solid jab on its chin. The snake coils back, hissing and reeling from the blow."
                + "\n\n\"What the ssshit dude!'' The snake says." + '"' + " Ssssucker punching me like that wassss ssssupremely unssstylissssh. If you sssought a sssscrap you sssshould have ssssaid sssso." + '"'
                + "\n\nYou do have to admit that that was not the most sporting route you could have taken. Somewhat embarassed, you offer an apology to the snake."
                + "\n\n\"sorry man." + '"'
                + "\n\n\"Itssss all good." + '"' + "The snake says." + '"' + "Sssso are we sssstill doing thissss?" + '"'
                + "\n\n\"Yep" + '"' + " you say."
                + "\n\n\"Ssssshall we agree on Marquessssss of Queenssssbury rulessss?" + '"' + " The snake asks."
                + "\n\n\"Sure thing" + '"' + " you tell it."
                + "\n\nAfter about half an hour spent for setting up the ring and calling in a referee as well the judges, the match begins.While initially quite uncertain, it quickly becomes clear that you have the upper hand.With the snake lacking arms, it is completely incapable of punching and blocking, meaning all it can do is dodge. The fight is stopped in the fourth round since the snake is unable to fulfill the referee's requests to keep its arms up and is thus judged incapable of continuing to defend itself. You are awarded the victory. The snake returns home to train, hoping that with a year off it can come back stronger and revitilize its boxing career. You, on the other hand are named champion and have won. Since there is nothing left for you to do but continue your journey, you prepare to do just that.";
            nextPages = new int[] { 5 };
            pageText = new PageText("Event: Manly as a Mongoose", "Punch Out", bodyText, nextPages);
            cs481Final.Utility.PageTexts.Add(pageText);

            //4 Intermission variant 1, where the player had met the spider. Ideally this and its second variant can be merged if I find the time to set up conditional text.
            bodyText = "With the Snake defeated and the Spider on his way, you continue down the path. Before long, you realize that wherever this road goes is a bit far from a hotel. You turn back and head to a camping supplies shop you remember seeing awhile back."
                + "\n\nYou're a bit low on cash, but with careful exploitation of items on sale and coupon abuse to put a middle-class housewife to shame, you are able to afford the purchase of a backpack, a lightweight tent, a tarp, a sleeping bag, a sleeping pad, some good boots, firestarting gear, some food, and a portable backpacking stove. While you know that this is not an entirely accurate representation of what you would actually need, you are well aware of the concept of narrative compression and the fact that this is good enough. You pay the cashier, load up your backpack with your new purchases, and head back out."
                + "\n\nstop for night, do some thinking."
                + "\n\nThat line of thought exhausted, you sit back as your fire flickers in the dark. You finish putting away your cooking gear and are about ready to put your fire out for the night when you are interupted by a loud voice."
                + "\n\n\"hoooWHOOOOO THE HELL ARE YOU?\"";
            nextPages = new int[] { 6 };
            pageText = new PageText("Intermission: A Bit of Thinking", "A quick thought process", bodyText, nextPages);
            cs481Final.Utility.PageTexts.Add(pageText);

            //5 Intermission variant 2, where the player had not met the spider.
            bodyText = "With the Snake defeated you fasten on your new championship belt, which is a gaudy gold. Bedecked in the regalia of a champion, you continue down the path. Before long, you realize that wherever this road goes is a bit far from a hotel. You turn back and head to a camping supplies shop you remember seeing awhile back."
                + "\n\nThankfully you won a champion's purse along with your championship belt and are able to easily afford the purchase of a backpack, a lightweight tent, a tarp, a sleeping bag, a sleeping pad, some good boots, firestarting gear, some food, and a portable backpacking stove. While you know that this is not an entirely accurate representation of what you would actually need, you are well aware of the concept of narrative compression and the fact that this is good enough. You pay the cashier, load up your backpack with your new purchases, and head back out."
                + "\n\nstop for night, meet spider and have a chat. imagine philosophical content of your choice."
                + "\n\nOnce again alone, you sit back as your fire flickers in the dark. You finish putting away your cooking gear and are about ready to put your fire out for the night when you are interupted by a loud voice."
                + "\n\n\"hoooWHOOOOO THE HELL ARE YOU?\"";
            nextPages = new int[] { 6 };
            pageText = new PageText("Intermission: A New Friend", "Enter the Spider", bodyText, nextPages, 2);
            cs481Final.Utility.PageTexts.Add(pageText);

            //6 Enter the Owl
            bodyText = "\"Actually, I don't give a hoot!\" The Owl says before you can explain yourself. \"These are my woods, and you don't belong here!\""
                + "\n\n\"Sorry?\" You offer."
                + "\n\n\"oohhhhohoooohooooo You will be!\" The Owl exclaims. \"I'll keep you up all night! You won't sleep a hooting wink!\""
                + "\n\n\"Could you not act so foolishly childish?\" You ask the Owl."
                + "\n\n\"Me!!??\" The Owl exclaims. \"Foolish? I'm an Owl! I'm wise! How dare you!\""
                + "\n\n\"Sorry?\" You offer."
                + "\n\n\"You think to mock me!\" the Owl rants. \"I'll show you! Give me any word! If I don't know the definition I'll even let you be!\"";
            pageText = new PageText("Event: The Midnight Hunter", "Enter the Owl", bodyText, new SpecialPageOwl1(), 3);
            cs481Final.Utility.PageTexts.Add(pageText);

            //7 Owl caught cheating(no internet)
            bodyText = "\"hoooWhooot the hell.\" The Owl mumbles. The Owl makes strange movements with its wings. Due to the dark and the angle you're viewing at, you can't really make out what its doing but you could swear you saw a glow. The Owl's movements and muttering convey a steadily increasing frustration."
                + "\n\n\"Everything ok up there?\" You ask."
                + "\n\n\"Yes, yes...\" The Owl replies. \"Just give me a minute.\""
                + "You give the Owl a minute. Then another. Just as you are about to speak up, the Owl releases a hoot of frustration and slams its wings against the branch. Something flies free."
                + "\n\n\"hoooWHoooops\" The Owl cries, its eyes growing wide. A smartphone plummets from the tree tops, its glowing screen tracing an arch of light through the dark of night."
                + "\n\nIt lands with a soft thud. There is a long silence as you stare up at the bird."
                + "\n\n\"You uh, whooodn't mind passing that back up here?\" The Owl asks. You simply stare incredously at the animal."
                + "\n\n\"Um, terrible service on that thing.\" The Owl continues awkwardly, trying to fill an equally awkward silence. \"Couldn't get it to connect to the internet for the life of me\""
                + "\n\n\"So, you were cheating.\" You say."
                + "\n\n\"No!\" Says the Owl. \"I was just, uh, checking um...\""
                + "\n\n\"Yes, you were.\" You say."
                + "\n\n\"Maybe a bit.\" The Owl says. \"But not really, it doesn't count.\""
                + "\n\nOnce again you find yourself staring increduously at a bird."
                + "\n\n\"So, uh, phone please?\" The Owl asks."
                + "\n\n\"Only if you agree to go be a cheating sack of crap somewhere else.\" You demand."
                + "\n\n\"hoooWhoooo the hell do you think you are?\" The Owl asks indignantly." 
                + "\n\n\"The owner of a new phone, apprently.\" You inform it."
                + "\n\nThe Owl pauses, and for a moment you fear it will swoop down and attack you. Then it hangs its head in defeat."
                + "\n\n\"Fine\" The Owl says."
                + "\n\nYou kneel down and pick up the phone. You lift it up and the Owl swoops down, snatches it up, and flies off. As you watch it fly off the Spider returns to the camp."
                + "\n\n\"Who was that?\" The Spider asks."
                + "\n\n\"A total hooooting asshole\" You reply.";
            nextPages = new int[] { 14 };
            pageText = new PageText("Event: Caught in the Dark", "red-feathered", bodyText, nextPages);
            cs481Final.Utility.PageTexts.Add(pageText);

            //8 Owl Wins(word was found in dictionary)
            bodyText = "The Owl makes strange movements with its wings. Due to the dark and the angle you're viewing at, you can't really make out what its doing but you could swear you saw a glow."
                + "\n\n\"[word]: [definition].\" The Owl says smugly. \"You lose.\""
                + "\n\n\"Oh, well that's not good.\" You say."
                + "\n\n\"Not for you its not!\" The Owl cries gleefully. \"My hooting will keep you up the whoooooool night!\""
                + "\n\nFor the next couple hours, it does. Try as you might, the Owl's loud hooting is inescapable and you cannot get to sleep. Just as you are about to give up, you hear another voice outside your tent."
                + "\n\n\"So, who's this guy?\" The Spider asks you as you emerge from the tent."
                + "\n\n\"An owl.\" You reply."
                + "\n\n\"Yeah, kinda figured.\" The Spiders says. \"Is he going to shut up? Humans aren't nocturnal if I'm remembering right, you should be getting some sleep right about now.\""
                + "\n\n\"hoooWHoooooo are y-whooooooo to be so naive as to ass-whoooo-m that I, the Owl, would deign to ch-whoooooo-se silence?\" The Owl interrupts, loud as ever."
                + "\n\n\"Holy shit.\" The Spider says. \"Does he always talk like that?\""
                + "\n\n\"Yeah, more or less.\" You tell the Spider. The Spider raises its eyebrows slightly(somehow), and approaches the base of the Owl's tree."
                + "\n\n\"Hey man, could you go do that somewhere else? My friend is trying to sleep here.\" The Spider calls up to the Owl."
                + "\n\n\"hooohoooohooo, give me a moment to consider.\" The Owl calls back down"
                + "\n\n\"ok.\""
                + "\n\nThere is a slight pause, and you savor the hootless silence for the handful of seconds it lasts."
                + "\n\n\"hooohoooHow about fricking n-hoooo-pe!\" The Owl hoots out, launching straight into a new cacophany of hoots."
                + "\n\nThe Spider's eyes narrow."
                + "\n\n\"You seriously need to quit it.\""
                + "\n\n\"hooohoooohooo, what do y-whooooo plan to do about it?.\" The Owl mocks."
                + "\n\n\"Ever heard of a goliath birdeater?\" The Spider asks. \"They are one of the biggest breeds of spiders, big enough to even eat birds on rare occassions.\""
                + "\n\n\"hooohoooohooo, a threat!\" The Owl laughs. \"An owl is far too hoooo-oge to be the meal of an insect!"
                + "\n\n\"I'm an arachnid, not an insect.\""
                + "\n\n\"I don't give a hoot\" The owl replies. \"You all taste the same to me!\""
                + "\n\n\"Wow, he really doesn't have any redeeming qualities, does he?\" The Spider mutters to you. Turning back to the Owl, the Spider raises himself up to his full height."
                + "\n\n\"Thing is, I'd meet a goliath birdeater and say he was a bit small.\" The Spider says, his voice and fangs dripping with menace and venom. If he wasn't such a nice fellow, you reckon that you'd be rather terrified of him right now. You helpfully shine a flashlight onto him to help illuminate that the giant spider is in fact a giant spider."
                + "\n\n\"hooohoooohooly shit!\" The Owl cries. \"I'm out!\""
                + "\n\n\"You will r-hoooooo this night!\" You hear the Owl cry as it flies away. \"My vengeance will be hooooorrible!\""
                + "\n\n\"Wow.\" The Spider says. \"What a hooooting asshole\"";
            nextPages = new int[] { 14 };
            pageText = new PageText("Event: Couldn't Come Owlt on Top", "sometimes owl you can do is your best", bodyText, nextPages);
            cs481Final.Utility.PageTexts.Add(pageText);

            //9 Player Wins(word was not found in dictionary)
            bodyText = "The Owl makes strange movements with its wings. Due to the dark and the angle you're viewing at, you can't really make out what its doing but you could swear you saw a glow."
                + "\n\n\"hoooWhooot the hell.\" The Owl says. \"Did you just make that up?\""
                + "\n\n\"No\" You say."
                + "\n\n\"Ok then tell me what it means\" The Owl demands."
                + "\n\n\"No\" You say."
                + "\n\n\"So you did make it up then!\" The Owl exclaims."
                + "\n\n\"No\" You say. \"I thought it was you who had to tell definitions, not me.\""
                + "\n\n\"Yes, but I already failed.\" The Owl retorts. \"I don't think it real, so tell me what it means as proof\""
                + "\n\n\"So you admit you lost then?\" You ask the Owl."
                + "\n\n\"Yes, but- \""
                + "\n\n\"And the prize for my victory is you not bothering me anymore?\" You ask."
                + "\n\n\"...yes.\" The Owl replies."
                + "\n\n\"So shouldn't you be on your way out?\""
                + "\n\nThe Owl spread its wings wide, and you can see the burning anger in its eye. For a moment you fear that it is about to launch into an attack, but then the Owl turns and takes off."
                + "\n\n\"You will r-hoooooo this night!\" You hear the Owl cry as it flies away. \"My vengeance will be hooooorrible!\""
                + "\n\nAs you watch the Owl fly off the Spider returns to the camp."
                + "\n\n\"Who was that?\" The Spider asks."
                + "\n\n\"A total hooooting asshole\" You reply.";
            nextPages = new int[] { 14 };
            pageText = new PageText("Event: Wise Victor", "he actually fell for it", bodyText, nextPages);
            cs481Final.Utility.PageTexts.Add(pageText);

            //10 Secret Route. (secret word was entered into the Owl's Challenge, activating the Cat)
            bodyText = "The Owl makes strange movements with its wings. Due to the dark and the angle you're viewing at, you can't really make out what its doing but you could swear you saw a glow."
                + "\n\n\"Grandmother: the mother of one's father or mother..\" The Owl says smugly. \"You lose.\""
                + "\n\n\"Oh, well that's not good.\" You say."
                + "\n\n\"Not for you its not!\" The Owl cries gleefully. \"My hooting will keep you up the whoooooool night!\""
                + "\n\nThere is a soft sound of metal. The Owl turns, and you simply stare. Behind the Owl is a cat. Its fur is a blue-gray, with a shimmering to it not unlike silk. One eye is blank void, a dull blindness. The other is a cold blue, sapphire ice full of cunning disdain. You did not think to see the Cat here."
                + "\n\n\"hoooWhooo-\" The Owl begins, but it is cut off as claws unsheath and the Cat explodes forth like a blued-steel missile. Talon and claws flash, and a chorus of hoots and yowls seek to drown each other out."
                + "\n\nBlood and feathers tumble through the air as the Owl takes to the skies."
                + "\n\n\"Scre-whooooo this!\" The Owl cries as it vanishes into the night."
                + "The Cat drops to the forest floor, its landing a whisper. As it is a cat, the leap is little problem to it."
                + "\"What are you doing here?\" you ask it, unsure of how the creature happened to be here of all places."
                + "Your only answer is a one-eyed stare of indifference, and then the Cat vanishes into the woods. Shaking your head, you figure its time to get some sleep.";
            nextPages = new int[] { 11 };
            pageText = new PageText("Event: Unexpected Savior", "where from though?", bodyText, nextPages, 4);
            cs481Final.Utility.PageTexts.Add(pageText);

            /*
            pages 11, 13, and 15 aren't complete in this version. They were originally conceived as a tribute to my grandmother, but with the deadline closing in I feel I do not have the time to write them in a way that I will be happy with.
            I refuse to half-ass them or phone them in, so I'm leaving them blank. I understand that this may lower my grade, but I accept that price.

            If I finish them out and add cut features like weather recognition, text parsing, and file i/o, can I also turn that in for the extra credit homework?
            */

            //11 secret route 1
            bodyText = "";
            nextPages = new int[] { 12, 13 };
            pageText = new PageText("Event: ", "", bodyText, nextPages);
            cs481Final.Utility.PageTexts.Add(pageText);

            //12 secret route leave(aborts secret route). Both this and "secret route continue" should be seen as good. Neither is right or wrong. Both make peace and accept loss, the protagonist just takes different roads to do so.
            bodyText = "You pause for a moment, and take a deep breath. You were in a forest, at night."
                + "\n\nIt was not day, you were not here."
                + "\n\nThere is nothing for you in this house. All that will greet you when you step over that threshhold is sorrow. You reach out and place a hand against the door. You feel battered wood dulled by aging paint press against your palm, slightly warm from the sun. You take another breath, and close your eyes for a moment."
                + "\n\nWhen you open them again you withdraw your palm. You tell yourself your peace is made, and you think that you might even believe it. You turn around. You walk away. Towards the street with its too-steep slope. When you reach the curb you pause for a third time, hoping against reason to hear someone shuffling after you to say goodbye. Hoping that grandmother and grandchild can say their farewells."
                + "\n\nThere is only silence."
                + "\n\nYou look on the old house, understanding this may very well be the last time you see it. You say your farewells, for your own sake and that of the dead."
                + "\n\nYou tell yourself your peace is made, and this time its true.";
            nextPages = new int[] { 14 };
            pageText = new PageText("Event: A Dream Denied", "and a peace made", bodyText, nextPages);
            cs481Final.Utility.PageTexts.Add(pageText);

            //13 secret route continue. Both this and "secret route leave" should be seen as good. Neither is right or wrong. Both make peace and accept loss, the protagonist just takes different roads to do so.
            bodyText = "";
            nextPages = new int[] { 15 };
            pageText = new PageText("Event: ", "", bodyText, nextPages, 5);
            cs481Final.Utility.PageTexts.Add(pageText);

            //14 morning
            bodyText = "Wake up, have breakfast."
                + "\n\nSpider chat." 
                + "\n\nJourney continues.";
            nextPages = new int[] { 16 };
            pageText = new PageText("Event: Morning Glory", "before the next move", bodyText, nextPages);
            cs481Final.Utility.PageTexts.Add(pageText);

            //15 morning secret route variant
            bodyText = "\"Who was that?\" The Spider asks."
                + "\n\n\"\"";
            nextPages = new int[] { 16 };
            pageText = new PageText("Event: ", "", bodyText, nextPages);
            cs481Final.Utility.PageTexts.Add(pageText);

            //16 Snake part 2/End
            bodyText = "You come across a familiar sight. A giant snake sits astride your path."
                + "\n\n\"Sssssup dude?\" The Snake says upon your approach."
                + "\n\n\"Not much, just looking for the end.\" You tell the Snake."
                + "\n\n\"You already found it. The Creditsssss rolled when that owl wasssssss handled.\" The Snake informs you."
                + "\n\nYou turn to the Spider. \"Now what then?\" you ask."
                + "\n\n\"We could always start over from the beggining.\" The Spider suggests. \"Surely there's more to see.\""
                + "\n\n\"Certainly an option.\" The Snake says. \"But maybe we sssssshould tell our own talesssss.\""
                + "\n\nBut that won't happen here, this ending is another beginning. Back to the start then?";
            nextPages = new int[] { 0 };
            pageText = new PageText("Event: An End", "for the next beginning", bodyText, nextPages);
            cs481Final.Utility.PageTexts.Add(pageText);
        }

        //are we connected to the internet?
        public static bool CheckConnected()
        {
            return CrossConnectivity.Current.IsConnected;
        }
    }

    //this whole mess can decode jsons taken from the owlbot thing. Unless otherwise noted, all of this is generated via the "quicktype" source from the week 9 powerpoint. I have no hand in its creation outside of pasting a json and playing with namespaces.
    public partial class WordDefinition
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("definition")]
        public string Definition { get; set; }

        [JsonProperty("example")]
        public string Example { get; set; }
    }

    public partial class WordDefinition
    {
        public static WordDefinition[] FromJson(string json) => JsonConvert.DeserializeObject<WordDefinition[]>(json, cs481Final.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this WordDefinition[] self) => JsonConvert.SerializeObject(self, cs481Final.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
    //end of quicktype generated code
}
