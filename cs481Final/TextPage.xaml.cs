﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace cs481Final
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TextPage : ContentPage
    {
        public PageText currentPageText; // the current text page

        //default constructor
        public TextPage()
        {
            InitializeComponent();
            Utility.currentTextPage = this;
        }

        //constructor
        public TextPage(int pageId)
        {
            InitializeComponent();
            Utility.currentTextPage = this;
            BuildTextPage(pageId);
        }

        //builds a text page
        public void BuildTextPage(int pageId)
        {
            currentPageText = Utility.PageTexts[pageId];
            BindingContext = currentPageText;
            Title = currentPageText.Title;

            //if character is unlocked, unlocks them
            if (currentPageText.UnlocksCharacter)
                Utility.GameCharacters[currentPageText.UnlockedCharacter].Met = true;
        }

        //go to the character page
        private async void CharListButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CharactersPage());
        }

        //continue has been hit
        private async void Continue_Clicked(object sender, EventArgs e)
        {

            if (currentPageText.ToSpecialPage) //do we go to a "special page"
                await Navigation.PushAsync(currentPageText.SpecialPage); //go to it.
            else if (currentPageText.NextPageTexts.Length == 1) //do we have no options?
                BuildTextPage(currentPageText.NextPageTexts[0]); //go to next page
            else
            {
                string[] options = new string[currentPageText.NextPageTexts.Length]; //declare the options array

                for (int i = 0; i < currentPageText.NextPageTexts.Length; i++) //build the options array
                    options[i] = "Option " + (i + 1);

                string action = await DisplayActionSheet("What do you do?", "Cancel", null, options); //display the actionsheet

                if (action != "Cancel") //if not canceling out, go the page
                {
                    action = action.Replace("Option ", "");
                    BuildTextPage(int.Parse(action));
                }
            }
        }

        
    }
}