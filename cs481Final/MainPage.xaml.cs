﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
/*
*/ 
namespace cs481Final
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            ConnectionCheck(); //internet?
        }

        //we intend to make use of the owlbot dictionary and maybe that weather one. We'll make sure things keep running smoothly if there isn't internet. I actually suggest doing a seperate playthrough with internet off, cause some stuff is different.
        void ConnectionCheck()
        {
            if (!Utility.CheckConnected())
                DisplayAlert("A Quick Network Warning", "This story is best experienced with an active network connection. While the story will be playable without it, some features will not work the way they were intended.", "Thanks for the heads up");
        }

        //go to the characters page
        private async void CharListButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CharactersPage()); 
        }

        //go to the first text page
        private async void StartButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new TextPage(0)); //passes data via constructor
        }
    }
}
