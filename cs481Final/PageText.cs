﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace cs481Final
{
    //text that goes in pages. This is for the less complex pages. If we can get a text parser running though....
    public class PageText
    {
        private string title;
        private string subtitle;
        private string bodyText;

        private int[] nextPageTexts;

        private bool unlocksCharacter; //do we unlock a character?
        private int unlockedCharacter; //index of character to unlock

        private bool toSpecialPage; //do we go to a special page?
        private Xamarin.Forms.Page specialPage; //special page to go to.

        //default constructor
        public PageText()
        { }

        //basic constructor
        public PageText(string title, string subtitle, string bodyText, int[] nextPageTexts)
        {
            this.title = title;
            this.subtitle = subtitle;
            this.bodyText = bodyText;
            this.nextPageTexts = nextPageTexts;

            this.unlocksCharacter = false;
            this.toSpecialPage = false;
        }

        //to special page constructor
        public PageText(string title, string subtitle, string bodyText, Xamarin.Forms.Page specialPage)
        {
            this.title = title;
            this.subtitle = subtitle;
            this.bodyText = bodyText;

            this.unlocksCharacter = false;

            this.toSpecialPage = true;
            this.specialPage = specialPage;
        }

        //basic constructor with character unlock
        public PageText(string title, string subtitle, string bodyText, int[] nextPageTexts, int unlockedCharacter)
        {
            this.title = title;
            this.subtitle = subtitle;
            this.bodyText = bodyText;
            this.nextPageTexts = nextPageTexts;

            this.unlocksCharacter = true;
            this.unlockedCharacter = unlockedCharacter;

            this.toSpecialPage = false;
        }

        //to special page constructor with character unlock
        public PageText(string title, string subtitle, string bodyText, Xamarin.Forms.Page specialPage, int unlockedCharacter)
        {
            this.title = title;
            this.subtitle = subtitle;
            this.bodyText = bodyText;

            this.unlocksCharacter = true;
            this.unlockedCharacter = unlockedCharacter;

            this.toSpecialPage = true;
            this.specialPage = specialPage;
        }

        public string Title { get => title; set => title = value; }
        public string Subtitle { get => subtitle; set => subtitle = value; }
        public string BodyText { get => bodyText; set => bodyText = value; }
        public int[] NextPageTexts { get => nextPageTexts; set => nextPageTexts = value; }
        public bool UnlocksCharacter { get => unlocksCharacter; set => unlocksCharacter = value; }
        public int UnlockedCharacter { get => unlockedCharacter; set => unlockedCharacter = value; }
        public bool ToSpecialPage { get => toSpecialPage; set => toSpecialPage = value; }
        public Page SpecialPage { get => specialPage; set => specialPage = value; }
    }
}
