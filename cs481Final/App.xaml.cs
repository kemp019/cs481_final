﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace cs481Final
{
    public partial class App : Application
    {
        //ideally, this only runs once, when the app is opened. That way we can use it to initialize things like Utility. 
        public App()
        {
            InitializeComponent();
            
            Utility.BuildGameCharacters();
            Utility.BuildGameTexts();

            MainPage = new NavigationPage( new MainPage());
            
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
