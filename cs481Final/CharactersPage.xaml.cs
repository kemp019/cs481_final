﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace cs481Final
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CharactersPage : TabbedPage
    {
        //dynamically create character pages that the player has unlocked.
        public CharactersPage()
        {
            InitializeComponent();

            //inefficient, but we know the characters list will never get to be meaningfully long, so we can do this the easy way and move on to more important stuff.
            for (int i = 0; i < Utility.GameCharacters.Count; i++)
            {
                if (Utility.GameCharacters[i].Met)
                    Children.Add(new CharacterPage(Utility.GameCharacters[i]));
            }
        }
    }
}