﻿using System;
using System.Collections.Generic;
using System.Text;

namespace cs481Final
{
    //game characters
    public class GameCharacter
    {
        private bool met; //has this character been met yet?
        private string name; //what is this character's name?
        private string subtitle; //character's subtitle for the characters screen
        private string description; //character's description for the characters screen

        //default constructor
        public GameCharacter()
        {
        }

        public GameCharacter(bool newMet, string newName, string newSubtitle, string newDescription)
        {
            met = newMet;
            name = newName;
            subtitle = newSubtitle;
            description = newDescription;
        }

        public bool Met { get => met; set => met = value; }
        public string Name { get => name; set => name = value; }
        public string Subtitle { get => subtitle; set => subtitle = value; }
        public string Description { get => description; set => description = value; }
    }
}
